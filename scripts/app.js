'use strict';

/**
 * @ngdoc overview
 * @name addixApp
 * @description
 * # addixApp
 *
 * Main module of the application.
 */

angular
  .module('addixApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'pascalprecht.translate',
    'vesparny.fancyModal',
    'angular-inview'
  ])
  .config(function ($routeProvider,$translateProvider,$locationProvider) {

      $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
      });

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
        // Lang switcher
        .when('/fr', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl',
            controllerAs: 'main'
        })
        .when('/en', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl',
            controllerAs: 'main'
        })
        .when('/nl', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl',
            controllerAs: 'main'
        })
        .when('/it', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl',
            controllerAs: 'main'
        })
      .otherwise({
        redirectTo: '/'
      });
      
    $translateProvider
        .useSanitizeValueStrategy(false)
        .useStaticFilesLoader({
            files: [{
                prefix: 'locales/',
                suffix: '.json'
            }]
        })
        .fallbackLanguage('de')
    
  }).run(function ($translate,$cookieStore,$rootScope,$window) {


        var lang = "de"
        var urlArray = window.location.pathname.split('/');
        $rootScope.languages = ['de','en','fr','it','nl'];

        for (var i = 0; i < $rootScope.languages.length; i++){
            if (urlArray.includes($rootScope.languages[i])){
                lang = $rootScope.languages[i]
            }
        }

        remove($rootScope.languages,lang);
        $rootScope.selectedLanguage = lang;





        $translate.use($rootScope.selectedLanguage);

        $rootScope.changeLanguage = function (code) {
            window.location.href = "/addix-compound/"+code;


            $rootScope.openLanguage();
            remove($rootScope.languages,code);
            var tmp = $rootScope.selectedLanguage;
            $translate.use(code);
            $cookieStore.put("language", code);
            $rootScope.selectedLanguage = code;
            $rootScope.languages.push(tmp);



            $( "#current-language" ).toggleClass( "open" );
            $rootScope.centerClaimPosition();

        };

        function remove(arr, what) {
            var found = arr.indexOf(what);

            while (found !== -1) {
                arr.splice(found, 1);
                found = arr.indexOf(what);
            }
        }

        $rootScope.openLanguage = function () {
            $( "#languages ul" ).slideToggle( "slow");
        };

        $.fn.scrollView = function () {
            return this.each(function () {
                $("html, body").animate({
                    scrollTop: $(this).offset().top
                },800);
            })
        };

    $rootScope.centerClaimPosition = function () {
            if($("body").width()>600) {
                var textWidth = (-($("#section-start").find(".text").width()) / 2) + 80;
                $(".text").css("margin-left", textWidth);
            }
        };



});
