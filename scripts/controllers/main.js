'use strict';

/**
 * @ngdoc function
 * @name addixApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the addixApp
 */
angular.module('addixApp')
   .directive('itemInit', function ($compile) {
        return function (scope, element, attrs) {
            scope.initItem(element);
        };
    })
  .controller('MainCtrl', function ($http,$scope,$fancyModal,$rootScope,$timeout,$cookieStore,$translate) {

      $scope.tire = [];
      $scope.tireLimit = 4;
      $scope.tireMaxLimit = 16;
      $scope.moveDelay = 120;

      $rootScope.area = null;
      $rootScope.tire = null;
      $scope.hasMoved = false;

      $scope.test = false;

      var intro = $("video");

      $timeout(function () {
          $rootScope.centerClaimPosition();
      },500);

      $(window).resize(function(){
          if($("body").width()<600){
              $(".text").addClass("text-mobile");
              setTimeout(function(){
                  $("body").removeClass("overflow-hidden");
              },100)
          }else
              $(".text").removeClass("text-mobile");
      });


      intro.on('ended',function(){
          $(".fullscreen-bg").fadeOut(2000);
          $("body").removeClass("overflow-hidden");
      });

      $scope.initItem = function(el){
          if($scope.test == false){
              $timeout(function () {
                  angular.element(el).addClass('visible');
              }, 5);
          }else{

          }
      };

      $scope.scrollTo = function(section) {
          $(section).scrollView();
      }

      $scope.flagsMoveDown = function () {
          if(!$scope.hasMoved){
              var time = 100;
              $(".flag-img").each(function(i,item){
                  time += $scope.moveDelay;
                  var foundItem = $("#flags").find(item);
                  move(foundItem,time)
                  $scope.hasMoved = true;
              });
          }
      };
      
      function move(e,time) {
          $timeout(function () {
            e.addClass("translate-null");
          },time)
      }


      $http.get("en/content/tire.json")
          .then(function (result) {
              var data = result.data;

              var i;
              $scope.allTire = [];

              for (i = 0; i < data.length; i++) {

                  var gotName = data[i].name;
                  var gotCompounds = data[i].compounds;
                  var gotLink = data[i].link;


                  var name = gotName.replace(new RegExp(" ", 'g'), "_");
                  var link = gotLink.replace(new RegExp(" ", 'g'), "_");
                  var img = gotName.replace(new RegExp(" ", 'g'), "_") + ".png";
                  var title = gotName.replace(new RegExp(" ", 'g'), "_") + "_title.png";
                  $scope.allTire[i] = {
                      'img' : "images/tire/" + img,
                      'title' : "images/tire/" + title,
                      'name' : name,
                      'link' : link,
                      'compounds' : gotCompounds
                  }
              }
          });

    $scope.loadInterview = function (interviewID) {

        $( interviewID ).toggleClass( "open" );
        $(interviewID).find(".interview-text").slideToggle("slow");
    };

    $scope.clickArea = function (area) {
        $rootScope.area = area;
        $fancyModal.open({
            templateUrl: 'views/area.html',
        });
    };

      $scope.clickTire = function (tire) {
          $rootScope.tire = tire;
          $fancyModal.open({
              templateUrl: 'views/tire.html',
              controller: ['$scope', function($scope) {
                      $scope.data = tire;
              }]

          });
      };

    $scope.loadMoreTire = function () {
        $("#load-more-text").toggleClass( "open" );
        if($scope.tireLimit == 4){
            $scope.tireLimit = $scope.tireMaxLimit;
            $timeout(function () {
                $scope.test = true;
            }, 10);
        }
        else{
            $scope.tireLimit = 4
            $scope.test = false;
        }


    }

  });

$(document).on("click","#current-language",function () {
    $( "#languages ul" ).slideToggle( "slow");
    $( "#current-language" ).toggleClass( "open" );
});



// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

document.addEventListener('touchmove', function(event) {
    event = event.originalEvent || event;
    if(event.scale > 1) {
        event.preventDefault();
    }
}, false);




//this is where we apply opacity to the arrow
$(window).scroll( function(){

    //get scroll position
    var topWindow = $(window).scrollTop();
    //multipl by 1.5 so the arrow will become transparent half-way up the page
    var topWindow = topWindow * 1.5;

    //get height of window
    var windowHeight = $(window).height();

    //set position as percentage of how far the user has scrolled
    var position = topWindow / windowHeight;
    //invert the percentage
    position = 1 - position;

    //define arrow opacity as based on how far up the page the user has scrolled
    //no scrolling = 1, half-way up the page = 0
    $('.arrow_down').css('opacity', position);

});

