'use strict';

/**
 * @ngdoc function
 * @name addixApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the addixApp
 */
angular.module('addixApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    console.log("a");
  });
